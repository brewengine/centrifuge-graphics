import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.imagepacker.TexturePacker;
import com.badlogic.gdx.imagepacker.TexturePacker.Settings;

public class CentrifugeTexturePacker
{
	private static final String ASSETS_PATH = "centrifuge-android-common/assets";
	private static final String ASSETS_JAVA_PATH = "centrifuge/src/com/brewengine/centrifuge";
	private static final String ASSETS_JAVA_PACKAGE = "com.brewengine.centrifuge";

	public static void main(String[] args) throws Exception
	{
		Settings settings = new TexturePacker.Settings();
		
		settings.alias = true;
		settings.alphaThreshold = 0;
		settings.debug = false;
//		settings.defaultFilterMag = default is Nearest
//		settings.defaultFilterMin = default is Nearest
//		settings.defaultFormat = default is RGBA8888
		settings.duplicatePadding = true;
		settings.incremental = false; // appends to existing pack, don't want
		settings.maxWidth = 1024;
		settings.maxHeight = 512;
		settings.minHeight = 16;
		settings.minWidth = 16;
		settings.padding = 1;
		settings.pot = true; // output to be power of two
		settings.rotate = false;
		settings.stripWhitespace = false;
		
		String graphicsDir = System.getProperty("user.dir") + File.separator + "graphics"+ File.separator;
		String input  = "in";
		String output = "out";
		
		File inDir = new File(graphicsDir, input);
        File[] inFiles = inDir.listFiles();
        for (File file : inFiles)
		{
        	if ( file.isDirectory() )
        	{
        		doTheInAndOut(
        			settings,
        			input  + File.separator + file.getName(),
        			output + File.separator + file.getName(),
        			graphicsDir,
        			file.getName()
        		);
        	}
		}
        
        System.out.println("Fin. Don't forget to REFRESH!! It's so refreshing!");
	}

	private static void doTheInAndOut(Settings settings, String input,
			String output, String dir, String in) throws FileNotFoundException, IOException
	{
		System.out.println("input: " + dir + input + " -> output: " + output);
		System.out.println("in: " + in);
		
		clearExistingOutDir(dir, output);
		TexturePacker.process(settings, dir + input, dir + output);
		
		File outDir = new File(dir, output);
		
		File packFile = changePNGFileNames(outDir);
	    changeFileNamesInPackFile(outDir, packFile);
	    String className = "Assets" + in.substring(0, 1).toUpperCase() + in.substring(1) + "Generated";
        createAssetsFile(dir + input, outDir, className, in);
        
        String assetsFileName = "Assets.java";
		File assetsFile = new File(outDir, assetsFileName);
		
		moveFiles(output, outDir, className + ".java", assetsFile, in);
	}

	private static void moveFiles(String output, File outDir,
			String assetsFileName, File assetsFile, String in)
	{
		File root = new File(System.getProperty("user.dir")).getParentFile();
        File movedAssetsFile = new File(root, ASSETS_JAVA_PATH + File.separator + assetsFileName);
		assetsFile.renameTo(movedAssetsFile);
		System.out.println("Outfile: " + movedAssetsFile.getAbsolutePath());
		
		for (File file : outDir.listFiles() )
		{
			File moveToFile = new File(root, ASSETS_PATH + File.separator + in + File.separator + file.getName());
			file.renameTo(moveToFile);
			System.out.println("Outfile: " + moveToFile.getAbsolutePath());
		}
	}

	private static void createAssetsFile(String input, File outDir, String className, String in)
			throws IOException
	{
		File inDir = new File(input);
        File[] inFiles = inDir.listFiles();
        BufferedWriter out = new BufferedWriter(new FileWriter(new File( outDir, "Assets.java")));


        List<String> allLines = new ArrayList<String>();
        
        List<String> classBeginLines  = new ArrayList<String>();
        List<String> constantLines    = new ArrayList<String>();
        List<String> fieldLines    = new ArrayList<String>();
        List<String> methodBeginLines = new ArrayList<String>();
        List<String> textureLoadLines = new ArrayList<String>();
        List<String> finalLines       = new ArrayList<String>();

        classBeginLines.add("package " + ASSETS_JAVA_PACKAGE + ";");
        classBeginLines.add("");
        classBeginLines.add("import com.badlogic.gdx.graphics.g2d.TextureAtlas;");
        classBeginLines.add("import com.badlogic.gdx.graphics.g2d.TextureRegion;");
        classBeginLines.add("import com.badlogic.gdx.Gdx;");
        classBeginLines.add("");
        classBeginLines.add( "/* AUTO GENERATED FILE */" );
        classBeginLines.add("");
        classBeginLines.add( "public abstract class "+ className + " {" );
        
        fieldLines.add("\tpublic TextureAtlas textures;");
        
        methodBeginLines.add( "\tpublic void load() {" );
        methodBeginLines.add( "\t\ttextures = new TextureAtlas(Gdx.files.internal(\"" + in + "/textures.txt\"));" );
        
        
        for (File file : inFiles)
		{
			String name = file.getName();
			if ( name.endsWith("png"))
			{
				name = name.substring(0, name.indexOf("."));
				char[] charArray = name.toCharArray();
				String constantName = name.substring(0,1);
				boolean prevIsUpperCase = false;
				for (int i = 1; i < charArray.length; i++)
				{
					char c = charArray[i];
					if (Character.isUpperCase(c) && charArray[i-1] != '-')
					{
						if (  !prevIsUpperCase )
						{
							constantName = constantName + "_";
						}
						else if ( i < charArray.length  && !Character.isUpperCase(charArray[i+1] ) )
						{
							constantName = constantName + "_";
						}
						prevIsUpperCase = true;
					}
					else
					{
						prevIsUpperCase = false;
					}
					constantName = constantName + c;
				}
				
				
//				String constantName = new String(chars.toArray(Character[] chars));
				
				if ( name.contains("-"))
				{
					constantName = constantName.replace("-", "_");
				}
				
				String varName = Character.toLowerCase(name.charAt(0))+name.replace("-", "").substring(1)+"Region";
				fieldLines.add("\tpublic TextureRegion "+ varName + ";");
				textureLoadLines.add("\t\t"+varName + " = textures.findRegion("+ constantName.toUpperCase()+");");
				String lookupName = name;
				if (lookupName.contains("_"))
				{
					lookupName = lookupName.substring(0,lookupName.indexOf("_"));
				}
				constantLines.add( "\tpublic static final String " + constantName.toUpperCase() + " = \"" + lookupName + "\";" );
			}
		}
        
        finalLines.add( "\t}" );
        finalLines.add( "}" );
        
        allLines.addAll(classBeginLines  );
        allLines.add("");
        allLines.addAll(constantLines    );
        allLines.add("");
        allLines.addAll(fieldLines       );
        allLines.add("");
        allLines.addAll(methodBeginLines );
        allLines.add("");
        allLines.addAll(textureLoadLines );
        allLines.add("");
        allLines.addAll(finalLines       );
        
        for (String string : allLines)
		{
			out.write(string);
			out.newLine();
		}
        
        out.close();
	}

	private static File changePNGFileNames(File outDir)
	{
		File[] outputFiles = outDir.listFiles();
		
		
		List<String> fileNames = new ArrayList<String>();
		File packFile = null;
		for (File file : outputFiles)
		{
			if ( file.getName().equals("pack"))
			{
				packFile = file;
			}
			else if ( file.getName().endsWith("png"))
			{
				char counter = file.getName().charAt(file.getName().indexOf(".")-1);
				
				String newFileName = "textures" + counter + ".png";
				file.renameTo(new File( outDir, newFileName));
				fileNames.add(newFileName);
			}
		}
		return packFile;
	}

	private static void changeFileNamesInPackFile(File outDir, File packFile)
			throws FileNotFoundException, IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(packFile));
	    BufferedWriter out = new BufferedWriter(new FileWriter(new File( outDir, "textures.txt")));
	    String str;
	    while ((str = in.readLine()) != null) 
	    {
	        if ( str.contains(".png"))
	        {
	        	str = "textures" + str.charAt(str.indexOf(".")-1) + ".png";
	        }
	        out.write( str );
	        out.newLine();
	    }
	    in.close();
        out.close();
        packFile.delete();
	}

	private static void clearExistingOutDir(String dir, String output)
	{
		File outDir = new File(dir, output);
		File[] outputFiles = outDir.listFiles();
		if ( outputFiles != null )
		{
			for (File file : outputFiles)
			{
				boolean deleted = file.delete();
				if (!deleted)
				{
					System.out.println("Could not delete file in output dir: " + file.getName() );
				}
			}
		}
	}
}
